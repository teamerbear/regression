"""
Runs the assigned regression and classification exercises for Homework 2.
"""

from extractor import extractor
from gradient_descent import BatchGradientDescentClassifier, StochasticGradientDescentClassifier
import os
import numpy as np

if __name__ == "__main__":
    # Try to remove old plots. It's fine if there aren't any.
    try:
        os.remove('batch_cost.png')
        os.remove('stoch_cost.png')
    except OSError:
        pass

    # Extract the training and test data from the files.
    features, labels = extractor.extract("data/reg_train.csv", append_bias=True)
    test_features, test_labels = extractor.extract(
        "data/reg_test.csv", append_bias=True)

    # Train the Batch Gradient Descent classifier.
    batch_clf = BatchGradientDescentClassifier()
    batch_rate = 0.0125
    print("\nTraining Batch Gradient Descent Classifier with r = {}".format(
        batch_rate))
    batch_clf.train(features, labels, rate=batch_rate)
    print("\tLearned Weight Vector: {}\n".format(batch_clf.weight_vector))
    batch_acc_train = batch_clf.accuracy_score(features, labels)
    batch_acc_test = batch_clf.accuracy_score(test_features, test_labels)
    print(
        "Cost of Batch Gradient Descent weight vector:\n\tTrain: {}\n\tTest: {}\n".
        format(batch_acc_train, batch_acc_test))

    # Train the Stochastic Gradient Descent classifier.
    stoch_clf = StochasticGradientDescentClassifier()
    stoch_rate = 0.0005
    print("Training Stochastic Gradient Descent Classifier with r = {}".format(
        stoch_rate))
    stoch_clf.train(features, labels, rate=stoch_rate, threshold=5e-8)
    print("\tLearned Weight Vector: {}\n".format(stoch_clf.weight_vector))
    stoch_acc_train = stoch_clf.accuracy_score(features, labels)
    stoch_acc_test = stoch_clf.accuracy_score(test_features, test_labels)
    print(
        "Cost of Stochastic Gradient Descent weight vector:\n\tTrain: {}\n\tTest: {}\n".
        format(stoch_acc_train, stoch_acc_test))

    # Calculate the optimal weight vector.
    x = np.transpose(features)
    x_2 = np.dot(x, np.transpose(x))
    x_2_inv = np.linalg.inv(x_2)
    x_3 = np.dot(x_2_inv, x)
    optimal_w = np.dot(x_3, np.transpose(labels))
    print("Optimal Weight Vector: {}\n".format(optimal_w))

    # Calculate the cost of the optimal weight vector.
    optimal_cost_train = batch_clf.compute_cost(optimal_w, features, labels)
    optimal_cost_test = batch_clf.compute_cost(optimal_w, test_features,
                                               test_labels)

    print("Cost of optimal weight vector:\n\tTrain: {}\n\tTest: {}\n".format(
        optimal_cost_train, optimal_cost_test))
