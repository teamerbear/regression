"""
Implements the Batch Gradient Descent Algorithm
"""

from copy import deepcopy
import matplotlib.pyplot as plt
import numpy as np
import random


class GradientDescentClassifier(object):
    """
    Uses the Gradient Descent algorithm to learn a weight vector that minimizes
    the mean squred error for a linear classifier.
    """

    def __init__(self):
        """ Constructor """
        self.weight_vector = None
        self.feature_mat = None
        self.label_vec = None
        self.cost_data = None

    def train(self):
        """
        Trains the classifier. This method should be overriden in each
        derived implementation of this base class.
        """
        raise NotImplementedError(
            "train() being called from base class. Child should have overriden this method."
        )

    def predict(self, feature_vec):
        """
        Uses a learned weight vector to predict the label value of a given
        feature vector.
        """
        label = np.dot(self.weight_vector, feature_vec)
        return label

    def accuracy_score(self, test_features, test_labels):
        """
        Predicts the labels for a matrix of test features and computes the mean
        squared error as the evaluation metric.
        """
        return self.compute_cost(self.weight_vector, test_features, test_labels)

    def plot_cost(self, num_iterations, filename, plot_title):
        """
        Plots the evolution of the cost over time.

        Arguments:
            num_iterations:     The number of iterations it took for the
                                algorithm to converge.
            filename:           The name of the file to store the plot.
            plot_title:         The title to apply to the plot itself.
        """
        t = np.array(range(num_iterations))

        # Note that using plt.subplots below is equivalent to using
        # fig = plt.figure and then ax = fig.add_subplot(111)
        fig, ax = plt.subplots()
        ax.plot(t, self.cost_data)

        ax.set(xlabel='Time (iterations)',
               ylabel='Cost (error)',
               title=plot_title)
        ax.grid()

        fig.savefig(filename)

    def check_data(self, feature_matrix, label_vector):
        """
        Verifies that the data is in the correct format and that all dimensions
        match.
        """
        # Matrix should be m x n
        if 2 != len(feature_matrix.shape):
            raise ValueError(
                "The input feature matrix is more than two dimensional!")

        # Matrix should have more than one feature and more than one training
        # sample.
        if (1 >= feature_matrix.shape[0]) or (1 >= feature_matrix.shape[1]):
            raise ValueError(
                "The input feature matrix does not have valid dimensions!")

        # The number of rows in the matrix should be the same as the number of
        # labels.
        if len(label_vector) != feature_matrix.shape[0]:
            raise ValueError(
                "The number of rows in the input feature matrix does not match the number of labels!"
            )

    def compute_gradient(self, w_t):
        """
        Computes the gradient of the cost function with respect to each variable
        in the feature vectors.

        Arguments:
            w_t:    The weight vector to use in computing the gradient.
        Returns:
            A vector where each element is the gradient of the cost function
            with respect to the corresponding input variable.
        """
        gradient = np.zeros(len(self.weight_vector))

        for j in xrange(len(self.weight_vector)):
            s = 0.0  # sum
            for i in xrange(len(self.label_vec)):
                s -= (self.label_vec[i] - np.dot(self.weight_vector,
                                                 self.feature_mat[i])
                      ) * self.feature_mat[i][j]

            gradient[j] = s

        return gradient

    def compute_cost(self, w_t, features, labels):
        """
        Computes the mean squared error over the input samples given a specific
        weight vector.

        Arguments:
            w_t:        The weight vector to use when computing the cost.
            features:   The matrix of features to use in calculating the cost.
            labels:     The vector of labels corresponding to the feature data.
        Returns:
            The result of the cost function with the given weight vector.
        """
        cost = 0.0
        # For each training example
        for i in xrange(len(labels)):
            y = labels[i]
            x = features[i]  # vector
            val = y - np.dot(w_t, x)
            cost += val * val

        cost *= 0.5
        return cost


class BatchGradientDescentClassifier(GradientDescentClassifier):
    """
    Uses the Batch Gradient Descent method to minimize the weight vector.
    """

    def __init__(self):
        """ Constructor """
        super(BatchGradientDescentClassifier, self).__init__()

    def train(self, data_matrix, label_vec, rate=0.01, threshold=1e-6):
        """
        Trains the classifier.

        Arguments:
            data_matrix:    The matrix of training samples.
            label_vec:      The vector of labels corresponding to the training
                            samples.
            rate:           The learning rate, r.
            threshold:      The stopping threshold. Indicates how small the
                            difference has to be between the norm of one
                            iteration's weight vector and the next's.
        """
        # Check that the input data is valid.
        super(BatchGradientDescentClassifier, self).check_data(data_matrix,
                                                               label_vec)

        # If the data is good, set the instance variables.
        self.feature_mat = data_matrix
        self.label_vec = label_vec

        # Initialize all the bookeeping stuff. Note that the weight vector
        # is initialized to all zeros.
        it = 0
        self.cost_data = np.array([])
        feature_len = data_matrix.shape[1]
        self.weight_vector = np.zeros(feature_len)
        r = rate
        thresh = threshold
        diff_norm = 99999.9
        last_w = np.zeros(feature_len)

        # Now run through the iterations and learn the weight vector.
        while diff_norm > thresh:
            cost = super(BatchGradientDescentClassifier, self).compute_cost(
                self.weight_vector, self.feature_mat, self.label_vec)
            self.cost_data = np.append(self.cost_data, cost)

            grad_J = super(BatchGradientDescentClassifier,
                           self).compute_gradient(self.weight_vector)
            last_w = deepcopy(self.weight_vector)
            for i in xrange(len(self.weight_vector)):
                self.weight_vector[i] -= r * grad_J[i]

            diff_vec = self.weight_vector - last_w
            diff_norm = np.linalg.norm(diff_vec)

            # Make sure to adjust the learning rate.
            #r *= 0.25

            it += 1

        # Make a plot of the cost evolution.
        super(BatchGradientDescentClassifier, self).plot_cost(
            it, "batch_cost.png", "Batch Cost Evolution")


class StochasticGradientDescentClassifier(GradientDescentClassifier):
    """
    Uses the Stochastic Gradient Descent method to minimize the weight vector.
    """

    def __init__(self):
        """ Constructor """
        super(StochasticGradientDescentClassifier, self).__init__()

    def train(self, data_matrix, label_vec, rate=0.01, threshold=1e-6):
        """ Trains the classifier. """

        # Check that the input data is valid.
        super(StochasticGradientDescentClassifier, self).check_data(
            data_matrix, label_vec)

        # Initialize our random number generator.
        random.seed()

        # If the data is good, set the instance variables.
        self.feature_mat = data_matrix
        self.label_vec = label_vec

        # Initialize all the bookeeping stuff. Note that the weight vector
        # is initialized to all zeros.
        it = 0
        self.cost_data = np.array([])
        feature_len = data_matrix.shape[1]
        self.weight_vector = np.zeros(feature_len)
        r = rate
        thresh = threshold
        cost_diff = 99999.9

        # Now run through the iterations and learn the weight vector.
        num_samples = len(self.label_vec)
        last_cost = super(StochasticGradientDescentClassifier,
                          self).compute_cost(self.weight_vector,
                                             self.feature_mat, self.label_vec)
        while cost_diff > thresh:
            # Pick a random sample and use that to update the weight vector.
            sample_ind = random.randint(0, num_samples - 1)
            self.update_weight_vector(self.weight_vector, sample_ind, r)

            # Compute the cost of the updated weight vector.
            cost = super(StochasticGradientDescentClassifier,
                         self).compute_cost(self.weight_vector,
                                            self.feature_mat, self.label_vec)
            self.cost_data = np.append(self.cost_data, cost)
            cost_diff = abs(cost - last_cost)
            last_cost = cost

            it += 1

        # Make a plot of the cost evolution.
        super(StochasticGradientDescentClassifier, self).plot_cost(
            it, "stoch_cost.png", "Stochastic Cost Evolution")

    def update_weight_vector(self, w_t, sample_ind, rate):
        """
        Updates the weight vector based on the training sample at the given
        index.

        Arguments:
            w_t:            The weight vector to update.
            sample_ind:     The index of the training sample we want to use to
                            update our weight vector.
            rate:           The learning rate of the algorithm.
        """
        orig_w = deepcopy(w_t)
        for j in xrange(len(w_t)):
            y_i = self.label_vec[sample_ind]
            x_i = self.feature_mat[sample_ind]
            x_ij = self.feature_mat[sample_ind][j]
            w_t[j] = orig_w[j] + rate * (y_i - np.dot(orig_w, x_i)) * x_ij
